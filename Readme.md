# Tugas Kelompok 1 PPW

[![pipeline status][pipeline-badge]][commits-gl]

## Daftar isi

- [Daftar isi](#daftar-isi)
- [Anggota Kelompok](#anggota-kelompok)
- [Cerita Aplikasi](#cerita-aplikasi)
- [Fitur-fitur Aplikasi](#fitur-fitur-aplikasi)
- [Link Aplikasi](#link-aplikasi)

## Kelompok C10
Nama Anggota :
- Ahmad Randey Juliano Rasyid - 1906399285
- Garry Hanuga - 1906399184
- Ahsan Najmy Ramadhan Putra Aji - 1906398641
- Aji Putra Pamungkas - 1906398736

## Cerita Aplikasi

~**Simulasi COVID 19**~

Simulasi COVID 19 adalah aplikasi Role Playing bertema pandemi [COVID-19][covid-19]. Aplikasi ini memisalkan kita sebagai pengunjung website atau pemakai aplikasi menjadi seorang pengurus warga desa X yang berkewajiban untuk mendata setiap pengunjung desa X. 

COVID-19 merupakan virus mematikan yang dapat menular pada orang-orang di sekitar. Menurut survey, sebagian besar (sekitar 80%) orang yang terinfeksi berhasil pulih tanpa perlu perawatan khusus, dan sekitar 1 dari 5 orang yang terinfeksi COVID-19 menderita sakit parah dan kesulitan bernapas. Lansia dan orang-orang kondisi medis seperti tekanan darah tinggi, gangguan jantung dan paru-paru, diabetes, atau kanker memiliki kemungkinan lebih besar mengalami sakit lebih serius. Namun, siapa pun dapat terinfeksi COVID-19 dan mengalami sakit yang serius. 

Mula-mula semua warga desa X tidak ada yang terinfeksi COVID-19, sehingga untuk menghindari penularan COVID 19 yang bisa meresahkan warga sekitar, kita sebagai pengurus warga harus mendata semua pengunjung baik itu nama, asal daerah, usia, serta status COVID 19 apakah dia terinfeksi atau tidak. Apabila *dia* positif, maka kamu diwajibkan untuk mengarantina *dia* supaya warga desa tidak ada yang terinfeksi. Selain itu kamu juga bisa memonitor pengunjung-pengunjung desa baik itu yang memiliki status negatif dan juga positif.


## Fitur-fitur Aplikasi
1. Informasi seputar COVID-19 : 
   Fitur ini menampilkan informasi seputar COVID-19

2. Daftar pengunjung desa X :
   Fitur ini untuk mendata semua pengunjung desa X dengan atribut nama, asal daerah, usia, status COVID-19

3. Data pengunjung desa X : 
   Fitur ini menampilkan semua pengunjung desa X dengan atribut-atributnya

4. Presentase positif COVID-19 di desa X : 
   Fitur ini menampilkan presentase orang yang memiliki status COVID-19 positif di dalam desa X.


## Link Aplikasi

[https://(link-heroku)][link-heroku]


[pipeline-badge]: https://gitlab.com/aji.putra.pamungkas/tes-project/badges/master/pipeline.svg
[commits-gl]: https://gitlab.com/aji.putra.pamungkas/tes-project/-/commits/master
[covid-19]: https://www.who.int/indonesia/news/novel-coronavirus/qa-for-public
[link-heroku]: https://(link-heroku)

